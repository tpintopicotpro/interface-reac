-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Thomas
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-08 14:20
-- Created:       2021-12-08 14:20
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "user"(
  "idUser" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL,
  "email" VARCHAR(45) NOT NULL,
  "createdAT" DATETIME DEFAULT NULL,
  "modifiedAT" DATETIME DEFAULT NULL,
  "deletedAT" DATETIME DEFAULT NULL
);
CREATE TABLE "competences"(
  "idcompetences" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(45) NOT NULL,
  "content" TEXT NOT NULL
);
CREATE TABLE "criteres"(
  "idcriteres" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "description" TEXT NOT NULL,
  "competences_idcompetences" INTEGER NOT NULL,
  CONSTRAINT "fk_criteres_competences1"
    FOREIGN KEY("competences_idcompetences")
    REFERENCES "competences"("idcompetences")
);
CREATE INDEX "criteres.fk_criteres_competences1_idx" ON "criteres" ("competences_idcompetences");
CREATE TABLE "user_has_criteres"(
  "user_idUser" INTEGER NOT NULL,
  "criteres_idcriteres" INTEGER NOT NULL,
  "idpivot" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  CONSTRAINT "fk_user_has_criteres_user1"
    FOREIGN KEY("user_idUser")
    REFERENCES "user"("idUser"),
  CONSTRAINT "fk_user_has_criteres_criteres1"
    FOREIGN KEY("criteres_idcriteres")
    REFERENCES "criteres"("idcriteres")
);
CREATE INDEX "user_has_criteres.fk_user_has_criteres_criteres1_idx" ON "user_has_criteres" ("criteres_idcriteres");
CREATE INDEX "user_has_criteres.fk_user_has_criteres_user1_idx" ON "user_has_criteres" ("user_idUser");
COMMIT;
