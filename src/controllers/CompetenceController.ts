import { Request, Response } from "express-serve-static-core";
//import { Tcompetences } from "../type";

export default class CompetencesController {
    static competences(req: Request, res: Response): void {
        const db = req.app.locals.db;
        const criteresAlreadyChecked: number[] = []
        const id = req.params.id;
        
        const competences = db.prepare('SELECT * FROM competences WHERE idcompetences = ?').all(id);

        const criteres = db.prepare('SELECT * FROM criteres WHERE competences_idcompetences = ?').all(id);

        const data = db.prepare('SELECT * FROM user_has_criteres WHERE user_idUser = ?').all(1);
        const recup = db.prepare('SELECT * FROM user_has_criteres').all();

        let arrCrit = [];
        data.forEach((element: any) => arrCrit.push(element.criteres_idcriteres));
        
        recup.forEach((element: any) => criteresAlreadyChecked.push(element.criteres_idcriteres));
        
        criteres.forEach((item: any) => {
            console.log (item)
            if(criteresAlreadyChecked.includes(item.idcriteres)){
                item.checked ='checked'
            }
            else{
                item.checked = ''
            }
        })
        
        res.render('pages/competences', { 
            title: 'Competences',
            competences: competences,
            criteres: criteres,
            checkbox: data,
            //checkbox : recup, 
        })
    }
}

/**
 * Faire une fonction qui permet d'afficher toutes les competences et leurs criteres une a une
 * @param req
 * @param res
 */
//     static programme (req: Request, res: Response): void {
//         const db = req.app.locals.db;
//         const stmt = db.prepare('SELECT * FROM competences').all()
//         const stmt2 = db.prepare('SELECT * FROM criteres').all()
//         //stmt.map (Elm => elm.checked = 'checked')
//         res.render('pages/programme', {
//             title: 'Competences',
//             competences: stmt ,
//             criteres: stmt2 ,
//         });
//     }
// }
