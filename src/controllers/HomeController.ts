import { Request, Response } from "express-serve-static-core";
import { appendFile } from "fs";
 
export default class HomeController
{

    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const stmt = db.prepare('SELECT * FROM competences WHERE idcompetences < 5').all();
        const stmt2 = db.prepare('SELECT * FROM competences WHERE idcompetences > 4').all();

        res.render('pages/index', {
            competence : stmt ,
            competence2: stmt2,
        });
    }

    static about(req: Request, res: Response): void
    {
        res.render('pages/about', {
            title: 'About',
        });
    }

    static register(req: Request, res: Response): void {
        res.render('pages/register', {
            
        })

    }
}