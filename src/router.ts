import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CompetencesController from "./controllers/CompetenceController";
import CriteresController from "./controllers/criteresController";

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) => {
        HomeController.about(req, res);
    });


    // affiche le form
    app.get('/competences/:id', (req, res) => {
        CompetencesController.competences(req, res);
    });

    // recup le form
    app.post('/criteres', (req, res) => {
        CriteresController.cocheUnCritere(req, res)
    });


    app.get('/register', (req, res) => {
        HomeController.register(req, res);
    });

    app.post('/register', (req, res) => {

        HomeController.register(req, res);
    });


    // app.get ('/programme', (req, res) =>{

    //     competences.programme (req, res)
    // });

    // app.post ('/competences/:id', (req, res) => {
    //     competences.competences (req, res)
    // });


    /** 
     * Tentative de CRUD:
    */
    // app.get('/AddArticle', (req, res) =>
    // {
    //     criteres.create(req, res)
    // });

    // app.post('/AddArticle', (req, res) =>
    // {
    //     criteres.create(req, res)
    // });
}